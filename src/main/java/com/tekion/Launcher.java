package com.tekion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import static com.tekion.Pools.springThreadPoolExecutor;
import static com.tekion.Pools.threadPoolExecutor;
import static java.lang.Thread.sleep;

/** @author amangulati (@amaan75.github) created on: 04/08/20 */
public class Launcher {

  private static class MyLongTask implements Callable<String> {
    @Override
    public String call() throws Exception {
      sleep(5000);
      return "done";
    }
  }

  public static void main(String[] args) throws InterruptedException {
    runJDKTasks();
    runSpringTasks();
  }

  private static void runSpringTasks() throws InterruptedException {
    List<Future<String>> tasks = new ArrayList<>();
    final ThreadPoolExecutor springThreadPool = springThreadPoolExecutor.getThreadPoolExecutor();
    Pools.printStats("PRE-START", springThreadPool);
    for (int index = 0; index < 50; index++) {
      final Future<String> submit = springThreadPoolExecutor.submit(new MyLongTask());
      tasks.add(submit);
    }
    Pools.printStats("POST-START", springThreadPool);
    for (int index = 0; index < 50; index++) {
      Thread.sleep(1000);
      Pools.printStats(String.valueOf(index), springThreadPool);
    }
  }

  private static void runJDKTasks() throws InterruptedException {
    List<Future<String>> tasks = new ArrayList<>();
    Pools.printStats("PRE-START", threadPoolExecutor);
    for (int index = 0; index < 50; index++) {
      final Future<String> submit = threadPoolExecutor.submit(new MyLongTask());
      tasks.add(submit);
    }
    Pools.printStats("POST-START", threadPoolExecutor);
    for (int index = 0; index < 50; index++) {
      Thread.sleep(1000);
      Pools.printStats(String.valueOf(index), threadPoolExecutor);
    }
  }
}
