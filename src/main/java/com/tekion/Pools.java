package com.tekion;

import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/** @author amangulati (@amaan75.github) created on: 04/08/20 */
public class Pools {

  public static final ThreadPoolExecutor threadPoolExecutor = threadPoolExecutor();

  public static final ThreadPoolTaskExecutor springThreadPoolExecutor =
      createThreadPoolTaskExecutor();

  private static ThreadPoolTaskExecutor createThreadPoolTaskExecutor() {
    final ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
    threadPoolTaskExecutor.setCorePoolSize(5);
    threadPoolTaskExecutor.setMaxPoolSize(50);
    threadPoolTaskExecutor.setKeepAliveSeconds(1);
    threadPoolTaskExecutor.setQueueCapacity(-1);
    threadPoolTaskExecutor.setThreadFactory(new CustomizableThreadFactory("SPRING-POOL"));
    threadPoolTaskExecutor.initialize();
    return threadPoolTaskExecutor;
  }

  private static ThreadPoolExecutor threadPoolExecutor() {
    return new ThreadPoolExecutor(
        5,
        50,
        1,
        TimeUnit.SECONDS,
        new SynchronousQueue<>(),
        new CustomizableThreadFactory("JDK-POOL"));
  }

  public static void printStats(String boundary, ThreadPoolExecutor executor) {
    final CustomizableThreadFactory threadFactory =
        (CustomizableThreadFactory) executor.getThreadFactory();
    System.out.println(
        "*************" + boundary + threadFactory.getThreadNamePrefix() + "**************");
    System.out.println("active count:" + executor.getActiveCount());
    System.out.println("core pool size:" + executor.getCorePoolSize());
    System.out.println("active pool size: " + executor.getActiveCount());
    System.out.println("current count:" + executor.getPoolSize());
    System.out.println("queue size :" + executor.getQueue().size());
  }
}
